package arya.afzar.torshiz.internetcafe.Models;

import com.google.gson.annotations.SerializedName;

public class Karfarma_Details {
    @SerializedName("ID")
    private  String ID;

    @SerializedName("name")
    private String name;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("phone")
    private String phone;

    @SerializedName("pic")
    private String pic;

    @SerializedName("status")
    private int status;

    public Karfarma_Details(String ID, String name, String username, String password, String phone, String pic, int status) {
        this.ID = ID;
        this.name = name;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.pic = pic;
        this.status = status;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
