package arya.afzar.torshiz.internetcafe.ClassHelper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


import arya.afzar.torshiz.internetcafe.MainActivity;
import arya.afzar.torshiz.internetcafe.R;

import static android.app.Activity.RESULT_OK;


public class Bottomsheet extends BottomSheetDialogFragment {
    private View view;
    ImageView gallery, camera, alaki;
    protected static final int request = 1;
    public static String picname = "";
    private static final int PICK_FILE_REQUEST = 100;
    private String selectedFilePath, ustring;
    ImageView imm;
    Bitmap bitmap1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogue_select_pic, container, false);
        camera = view.findViewById(R.id.camera);
        gallery = view.findViewById(R.id.gallery);
        alaki = view.findViewById(R.id.alaki);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    startActivityForResult(cameraintent, request);
                } catch (ActivityNotFoundException e) {
                    Snackbar.make(getView(), "متاسفانه برنامه دوربین یافت نشد", Snackbar.LENGTH_SHORT).show();
                }

            }
        });


        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "عکس خود را انتخاب کنید . . ."), PICK_FILE_REQUEST);
            }
        });
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == request && data != null && resultCode == RESULT_OK) {
            bitmap1 = (Bitmap) data.getExtras().get("data");
            //ایمیج ویو استفاده کنم
            alaki.setImageBitmap(bitmap1);
            save();
            if (MainActivity.type==1) {
            MainActivity.profile_logo_karfarma= selectedFilePath;
            MainActivity.logo_karfarma.setImageBitmap(bitmap1);
            }
            //ایمیج ویوی که قراره عکس رو نشون بده استاتیک کن اینجا بزار
            //یه رشته با عنوان selectedFilePath بزار استاتیک کن
            //==============================
            //camera
        } else if (resultCode == RESULT_OK) {

            if (requestCode == PICK_FILE_REQUEST) {

                if (data == null) {
                    return;
                }

                Uri selectedFileUri = data.getData();
                try {
                    bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedFileUri);
                    alaki.setImageBitmap(bitmap1);
                } catch (IOException e) {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                }
                selectedFilePath = selectedFileUri.getPath();

                String[] au = selectedFilePath.split(":");
                String as = au[1];
                compress();
                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    if (MainActivity.type == 1) {
                        MainActivity.profile_logo_karfarma = selectedFilePath;
                        MainActivity.logo_karfarma.setImageBitmap(bitmap1);
                    }
                    //اینجا هم همون قبلیا رو بکن
                } else {
                    Snackbar.make(getView(), "متاسفانه عکس به درستی انتخاب نشد ، عکس دیگری برگزینید", Snackbar.LENGTH_SHORT).show();
                }
            }
        }

    }

    private void compress() {

        int size = (int) (bitmap1.getHeight() * (812.0 / bitmap1.getWidth()));
        Bitmap bmp = Bitmap.createScaledBitmap(bitmap1, 300, size, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        selectedFilePath = Base64.encodeToString(bytes, 0);
    }

    private void save() {
        FileOutputStream outputStream = null;
//        try {
        //compress();
        int size = (int) (bitmap1.getHeight() * (812.0 / bitmap1.getWidth()));
        Bitmap bmp = Bitmap.createScaledBitmap(bitmap1, 300, size, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        selectedFilePath = Base64.encodeToString(bytes, 0);
        //getFilename();
    }

    private String getFilename() {

        File file = new File(Environment.getExternalStorageDirectory().getPath(), "novin_pic");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriString = (file.getAbsolutePath() + "/" + picname + ".jpg");
        return uriString;
    }
}
