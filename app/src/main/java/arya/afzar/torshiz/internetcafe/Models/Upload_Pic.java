package arya.afzar.torshiz.internetcafe.Models;

import com.google.gson.annotations.SerializedName;

public class Upload_Pic {

    @SerializedName("status")
    private String status;

    public Upload_Pic(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
