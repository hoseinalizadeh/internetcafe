package arya.afzar.torshiz.internetcafe;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import arya.afzar.torshiz.internetcafe.ClassHelper.Bottomsheet;
import arya.afzar.torshiz.internetcafe.ClassHelper.GetData;
import arya.afzar.torshiz.internetcafe.ClassHelper.RuntimePermissionsFragment;
import arya.afzar.torshiz.internetcafe.ClassHelper.sanjesh_hosein;
import arya.afzar.torshiz.internetcafe.Models.Karfarma_Details;
import arya.afzar.torshiz.internetcafe.Models.Upload_Pic;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends RuntimePermissionsFragment {


    //===============================
    //hosein
    ScrollView scrollView_signup_karfarma, scrollView_signin_karfarma;
    public static String profile_logo_karfarma = "";
    public static CircleImageView logo_karfarma;
    int Read_code = 300;
    EditText txtname_karfarma, txtusername_karfarma, txtpassword_karfarma, txtphone_karfarma;
    TextView ozvam;
    Button btn_select_pic_karfarma, btnsignup_karfarma;
    ProgressBar progressBar_karfarma_signup;
    Retrofit retrofit_karfarma;
    public static int type = 0;
    GetData getData_karfarma;
    String ch = "";
    SharedPreferences sharedPreferences;
    //=================================

    GetData getData_vorood;
    //============================
    //vorood
    EditText txtusername_vorood, txtpassword_vorood;
    Button btn_vorood;
    TextView txtozviat, txtforget_pass;
    CheckBox chkrememberme;
    //================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //=======================================
        //hosein
        finds();

        retrofit_karfarma = sanjesh_hosein.retrofit();
        sharedPreferences = getSharedPreferences("cafenet", MODE_PRIVATE);

        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");

        if (!username.isEmpty()) {
            Intent intent = new Intent(MainActivity.this, Karfarmasignup.class);
            Bundle bdn = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.anim, R.anim.anim2).toBundle();
            startActivity(intent, bdn);
            finish();

        } else {
            scrollView_signin_karfarma.setVisibility(View.VISIBLE);
        }


        Boolean isconnect = sanjesh_hosein.isNetworkConnected(this);
        if (!isconnect) {
            Toast.makeText(this, "لطفا دسترسی خود را به اینترنت چک کنید !", Toast.LENGTH_SHORT).show();
        } else {
            btn_select_pic_karfarma.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type = 1;
                    MainActivity.super.requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Read_code);
                }
            });
            btnsignup_karfarma.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signup_karfarma();
                }
            });

        ozvam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.SlideOutUp).duration(100).repeat(1).playOn(scrollView_signup_karfarma);
                scrollView_signup_karfarma.setVisibility(View.INVISIBLE);

                YoYo.with(Techniques.SlideInUp).duration(1000).repeat(1).playOn(scrollView_signin_karfarma);
                scrollView_signin_karfarma.setVisibility(View.VISIBLE);
            }
        });

        //=========================================
        //hosein
            //=============
            btn_vorood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signin();
                }
            });
        }
        txtozviat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.SlideOutUp).duration(100).repeat(1).playOn(scrollView_signin_karfarma);
                scrollView_signin_karfarma.setVisibility(View.INVISIBLE);

                YoYo.with(Techniques.SlideInUp).duration(1000).repeat(1).playOn(scrollView_signup_karfarma);
                scrollView_signup_karfarma.setVisibility(View.VISIBLE);
            }
        });

        txtforget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //code for phone to restore password
            }
        });


            //=======
    }

    private void signin() {

        String eror = "";
        Toast tost = Toast.makeText(this, eror, Toast.LENGTH_SHORT);
        if (txtpassword_vorood.getText().toString().trim().isEmpty()) {
            eror += " لطفا رمز ورود را خالی نگذارید!";
            tost.setGravity(Gravity.CENTER_VERTICAL, 0, 0);

            txtpassword_vorood.requestFocus();
        }
        if (txtusername_vorood.getText().toString().trim().isEmpty()) {
            eror += "\n لطفا رمز ورود را خالی نگذارید!";
            tost.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            txtusername_vorood.requestFocus();
        }
        if (!eror.isEmpty()) {
            tost.setText(eror);
            tost.show();
        } else {
            boolean internet = sanjesh_hosein.isNetworkConnected(this);
            if (internet == false) {
                eror += "\n لطفا دسترسی خود را به اینترنت چک کنید ";
            }
            if (!eror.isEmpty()) {
                Snackbar.make(findViewById(R.id.layer1), eror, Snackbar.LENGTH_SHORT).show();
            } else {
                progressBar_karfarma_signup.setVisibility(View.VISIBLE);
                getData_vorood = retrofit_karfarma.create(GetData.class);
                Call<List<Karfarma_Details>> login = getData_vorood.getshart("select * from user_karfarma where username='" + txtusername_vorood.getText().toString().trim() + "' and password = '" + txtpassword_vorood.getText().toString().trim() + "'");
                login.enqueue(new Callback<List<Karfarma_Details>>() {
                    @Override
                    public void onResponse(Call<List<Karfarma_Details>> call, Response<List<Karfarma_Details>> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().size() > 0) {
                                int status = response.body().get(0).getStatus();
                                if (status == 2) {
                                    Toast.makeText(MainActivity.this, "پروفایل شما محدود شده و شما می توانید فقط پروژه را انجام دهید", Toast.LENGTH_SHORT).show();
                                } else if (status == 3) {
                                    Toast.makeText(MainActivity.this, "پروفایل شما به دلیل گزارش تخلف کاربران مسدود شده است .", Toast.LENGTH_SHORT).show();
                                } else {
                                    progressBar_karfarma_signup.setVisibility(View.INVISIBLE);
                                    sharedPreferences.edit().putString("username", txtusername_vorood.getText().toString().trim()).apply();
                                    sharedPreferences.edit().putString("password", txtpassword_vorood.getText().toString().trim()).apply();

                                    Intent intent = new Intent(MainActivity.this, Karfarmasignup.class);
                                    Bundle bdn = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.anim, R.anim.anim2).toBundle();
                                    startActivity(intent, bdn);
                                    finish();
                                }

                            }
                            else {
                                Toast.makeText(MainActivity.this, "کاربری با این نام کاربری یافت نشد", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Karfarma_Details>> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "متاسفانه مشکلی در اتصال به سرور به وجود آمده ", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }


    }


    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == Read_code) {

            ShowFileChooser();
        }
    }


    @Override
    public void onPermissionsDeny(int requestCode) {
        Toast.makeText(this, "لطفا دسترسی به برنامه را جهت انتخاب عکس بدهید !", Toast.LENGTH_SHORT).show();
    }

    //======================
    //hosein
    private void finds() {
        logo_karfarma = findViewById(R.id.logo_karfarma);
        txtname_karfarma = findViewById(R.id.txtname_karfarma);
        txtusername_karfarma = findViewById(R.id.txtusername_karfarma);
        txtpassword_karfarma = findViewById(R.id.txtpassword_karfarma);
        txtphone_karfarma = findViewById(R.id.txtphone_karfarma);
        ozvam = findViewById(R.id.ozvam_karfarma);
        btn_select_pic_karfarma = findViewById(R.id.btn_select_pic_karfarma);
        btnsignup_karfarma = findViewById(R.id.btn_ozviat_karfarma);
        progressBar_karfarma_signup = findViewById(R.id.progressbar_karfarma_signup);

        txtusername_vorood = findViewById(R.id.txt_username_vorrod_karfarma);
        txtpassword_vorood = findViewById(R.id.txt_password_vorrod_karfarma);
        txtforget_pass = findViewById(R.id.forget_pass);
        txtozviat = findViewById(R.id.ozviat);
        btn_vorood = findViewById(R.id.btn_vorood_karfarma);
        scrollView_signin_karfarma = findViewById(R.id.scroll_vorood_karfarma);
        scrollView_signup_karfarma = findViewById(R.id.scroll_register_karfarma);

    }

    private void ShowFileChooser() {
        Bottomsheet bottomSheet = new Bottomsheet();
        bottomSheet.show(getSupportFragmentManager(), bottomSheet.getTag());
    }

    private void signup_karfarma() {
        String error = "";


        if (txtphone_karfarma.getText().toString().trim().length() < 11) {
            error += "\n  لطفا شماره همراه را به درستی وارد کنید ";
            txtphone_karfarma.requestFocus();
        }
        if (txtphone_karfarma.getText().toString().trim().isEmpty()) {
            error += "\n  لطفا شماره همراه را وارد کنید ";
            txtphone_karfarma.requestFocus();
        }
        if (!txtphone_karfarma.getText().toString().startsWith("09")) {
            error += "\n  لطفا شماره همراه را به درستی وارد کنید ";
            txtphone_karfarma.requestFocus();
        }
        if (txtpassword_karfarma.getText().toString().trim().isEmpty()) {
            error += "\n لطفا رمز عبور برای خود تعیین کنید ";
            txtpassword_karfarma.requestFocus();
        }
        if (txtpassword_karfarma.getText().toString().trim().length() < 4) {
            error += "\n رمز عبور نمی تواند کمتر از 4 حرف و بیشتر از 8 حرف باشد";
            txtpassword_karfarma.requestFocus();
        }
        if (txtusername_karfarma.getText().toString().trim().length() < 4) {
            error += "\n نام کاربری نمی تواند کمتر از 4 حرف و بیشتر از 10 حرف باشد";
            txtusername_karfarma.requestFocus();
        }
        if (txtusername_karfarma.getText().toString().trim().isEmpty()) {
            error += "\n نام کاربری نمی تواند خالی باشد";
            txtusername_karfarma.requestFocus();
        }
        if (profile_logo_karfarma.isEmpty()) {
            error += "\n عکسی برای پروفایل خود انتخاب کنید";

        }
        boolean internet = sanjesh_hosein.isNetworkConnected(this);
        if (internet == false) {
            error += "\n لطفا دسترسی خود را به اینترنت چک کنید ";
        }
        if (!error.isEmpty()) {
            Snackbar.make(findViewById(R.id.layer1), error, Snackbar.LENGTH_SHORT).show();
        } else {
            getData_karfarma = retrofit_karfarma.create(GetData.class);
            progressBar_karfarma_signup.setVisibility(View.VISIBLE);
            CheckUserNotInserted_karfarma();

        }
    }

    private void CheckUserNotInserted_karfarma() {
        Call<List<Karfarma_Details>> getuser = getData_karfarma.getshart("select * from user_karfarma where username='" + txtusername_karfarma.getText().toString().trim() + "' and password = '" + txtpassword_karfarma.getText().toString().trim() + "'");
        getuser.enqueue(new Callback<List<Karfarma_Details>>() {
            @Override
            public void onResponse(Call<List<Karfarma_Details>> call, Response<List<Karfarma_Details>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().size() > 0) {
                        String phone = response.body().get(0).getPhone();
                        String user = response.body().get(0).getUsername();
                        if (phone.equals(txtphone_karfarma.getText().toString().trim())) {
                            ch += "\n شماره تلفن قبلا ثبت شده است";
                        }
                        if (user.equals(txtusername_karfarma.getText().toString().trim())) {
                            ch += "\n نام کاربری قبلا ثبت شده است";
                        }
                        progressBar_karfarma_signup.setVisibility(View.INVISIBLE);
                        Snackbar.make(findViewById(R.id.layer1), ch.trim(), Snackbar.LENGTH_SHORT).show();

                    } else {
                        ch = "";
                        upload_pic_karfarma();
                        Call<List<Karfarma_Details>> insert_karfarma = getData_karfarma.insert("insert into user_karfarma (name,username,password,phone,pic,status)values('" + txtname_karfarma.getText().toString().trim() + "','" + txtusername_karfarma.getText().toString().trim() + "','" + txtpassword_karfarma.getText().toString().trim() + "','" + txtphone_karfarma.getText().toString().trim() + "','" + txtusername_karfarma.getText().toString().trim() + "_1" + "','" + 1 + "')");
                        insert_karfarma.enqueue(new Callback<List<Karfarma_Details>>() {
                            @Override
                            public void onResponse(Call<List<Karfarma_Details>> call, Response<List<Karfarma_Details>> response) {
                                if (response.isSuccessful()) {
                                    progressBar_karfarma_signup.setVisibility(View.INVISIBLE);
                                    Snackbar.make(findViewById(R.id.layer1), "با موفقیت ثبت شد", Snackbar.LENGTH_SHORT).show();
                                    sharedPreferences.edit().putInt("type", 1).apply();
                                    sharedPreferences.edit().putString("username", txtusername_karfarma.getText().toString().trim()).apply();
                                    sharedPreferences.edit().putString("password", txtpassword_karfarma.getText().toString().trim()).apply();

                                    Intent intent = new Intent(MainActivity.this, Karfarmasignup.class);
                                    Bundle bdn = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.anim, R.anim.anim2).toBundle();
                                    startActivity(intent, bdn);
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<Karfarma_Details>> call, Throwable t) {
                                Snackbar.make(findViewById(R.id.layer1), "متاسفانه استثنایی رخ داده ، لطفا چند لحظه بعد دوباره امتحان کنید", Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Karfarma_Details>> call, Throwable t) {

            }
        });
    }

    private void upload_pic_karfarma() {
        Call<Upload_Pic> upload = getData_karfarma.uploadpic(profile_logo_karfarma, txtusername_karfarma.getText().toString().trim() + "_1");
        upload.enqueue(new Callback<Upload_Pic>() {
            @Override
            public void onResponse(Call<Upload_Pic> call, Response<Upload_Pic> response) {
                if (response.isSuccessful()) {
                    String aa = response.body().getStatus();
                    if (aa.equals("yes")) {
                        //Snackbar.make(getView(), "با موفقیت ثبت شد", Snackbar.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<Upload_Pic> call, Throwable t) {
                Snackbar.make(findViewById(R.id.layer1), "متاسفانه در آپلود عکس خطایی مشاهده شد ، شما میتوانید \n  از قسمت ویرایش پروفایل اقدام به بارگزاری عکس کنید", Snackbar.LENGTH_SHORT).show();
            }
        });
    }
//=======================
//hosein
}

