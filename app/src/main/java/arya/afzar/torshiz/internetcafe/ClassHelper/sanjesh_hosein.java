package arya.afzar.torshiz.internetcafe.ClassHelper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class sanjesh_hosein {
    Context context;
    public sanjesh_hosein(Context context) {
        this.context = context;
    }

    public static boolean isNetworkConnected(Context context) { // check internet connection
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }


    public static Retrofit retrofit() {
        Retrofit mretrofit = new Retrofit.Builder().baseUrl(GetData.BaseUrl).addConverterFactory(GsonConverterFactory.create()).build();
        return mretrofit;
    }
}
