package arya.afzar.torshiz.internetcafe.ClassHelper;

import java.util.List;

import arya.afzar.torshiz.internetcafe.Models.Karfarma_Details;
import arya.afzar.torshiz.internetcafe.Models.Upload_Pic;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetData {
    String BaseUrl = "http://aryaafzartorshiz.ir/";


    @GET("register2.php")
    Call<List<Karfarma_Details>> getshart(@Query("namm") String s);


    @FormUrlEncoded
    @POST("insert.php")
    Call<List<Karfarma_Details>> insert(@Field("comment") String s1);


    @FormUrlEncoded
    @POST("upload2.php")
    Call<Upload_Pic> uploadpic(@Field("pic") String pic, @Field("title") String title);
}
